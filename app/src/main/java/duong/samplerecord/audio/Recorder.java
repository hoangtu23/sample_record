package duong.samplerecord.audio;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;

/**
 * Created by duonglh2 on 30/11/2016.
 */
public class Recorder {

    private MediaRecorder mRecorder;

    public Recorder() {
        mRecorder = null;
    }

    public void release() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
    }

    public void startRecording(String filePath) {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        mRecorder.setOutputFile(filePath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("duong_failed", "prepare failed!");
        }

        mRecorder.start();
    }

    public void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }
}
