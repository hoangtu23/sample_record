package duong.samplerecord.audio;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Papa on 30/11/2016.
 */
public class Player {

    private MediaPlayer mPlayer;

    public Player(String filePath) {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(filePath);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void prepare() {
        try {
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play() {
        mPlayer.start();
    }

    public void stop() {
        mPlayer.release();
        mPlayer = null;
    }

    public void pause() {
        mPlayer.pause();
    }

    public void setOnCompleteListener(MediaPlayer.OnCompletionListener listener){
        mPlayer.setOnCompletionListener(listener);
    }

    public int getDuration() {
        return mPlayer.getDuration();
    }

    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

}
