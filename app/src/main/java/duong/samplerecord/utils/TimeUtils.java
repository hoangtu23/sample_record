package duong.samplerecord.utils;

/**
 * Created by Papa on 01/12/2016.
 */
public class TimeUtils {

    public static String covertMillisecToString(int millisec) {
        String finalTimerString = "";
        String secondsString;

        // Convert total duration into time
        int hours = millisec / (1000*60*60);
        int minutes = millisec % (1000*60*60) / (1000*60);
        int seconds = millisec % (1000*60*60) % (1000*60) / 1000;
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }
}
