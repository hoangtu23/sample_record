package duong.samplerecord.model;

/**
 * Created by Papa on 01/12/2016.
 */
public class Record {

    private String path;
    private String name;
    private String duration;
    private double size;
    private boolean isPlaying;

    public Record(String path, String name, double size, String duration, boolean isPlaying) {
        this.path = path;
        this.name = name;
        this.size = size;
        this.duration = duration;
        this.isPlaying = isPlaying;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
