package duong.samplerecord.model;

/**
 * Created by tulh2 on 12/9/16.
 */

public class MessageEntity {
    String content;
    long time;

    public MessageEntity() {

    }

    public MessageEntity(String _content, long _time) {
        this.content = _content;
        this.time = _time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
