package duong.samplerecord.activity;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import duong.samplerecord.R;
import duong.samplerecord.audio.Recorder;
import duong.samplerecord.model.Record;
import duong.samplerecord.utils.Constants;
import duong.samplerecord.utils.TimeUtils;

public class RecordActivity extends AppCompatActivity {

    private ImageView btRecord;
    private ListView lvRecord;
    private Recorder recorder;
    private List<Record> records;
    private MediaPlayer player;
    private AudioAdapter adapter;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        initViews();
        records = getListRecords();
        adapter = new AudioAdapter(this, R.layout.item_audio_layout);
        lvRecord.setAdapter(adapter);
        recorder = new Recorder();

        btRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        //Finish record and save file
                        btRecord.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rec));
                        recorder.stopRecording();
                        records = getListRecords();
                        adapter.notifyDataSetChanged();
                        Toast.makeText(RecordActivity.this, "Stop record", Toast.LENGTH_SHORT).show();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        //Start record
                        btRecord.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rec_red));
                        String root = Environment.getExternalStorageDirectory().toString();
                        File myDir = new File(root + Constants.RECORD_FOLDER_PATH);
                        if (!myDir.exists()) myDir.mkdir();
                        int n = (int) System.currentTimeMillis();
//                        String filePathDesti = myDir.getAbsolutePath() + "/" + "Record" + n + ".3gp";
                        String filePathDesti = myDir.getAbsolutePath() + "/" + "Record" + n + ".aac";
                        recorder.startRecording(filePathDesti);
                        Toast.makeText(RecordActivity.this, "Start record", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
    }

    private List<Record> getListRecords() {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + Constants.RECORD_FOLDER_PATH);
        if (!myDir.exists()) myDir.mkdir();
        File[] files = myDir.listFiles();
        ArrayList<Record> records = new ArrayList<>();
        for (File file : files) {
            double size = (double) file.length()/1000;
            player = new MediaPlayer();
            int length = 0;
            try {
                player.setDataSource(file.getAbsolutePath());
                player.prepare();
                length = player.getDuration();
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.release();
            player = null;
            records.add(new Record(file.getAbsolutePath(), file.getName(), size
                    ,TimeUtils.covertMillisecToString(length), false));
        }
        return records;
    }

    private void initViews() {
        btRecord = (ImageView) findViewById(R.id.bt_record);
        lvRecord = (ListView) findViewById(R.id.lv_record);
    }

    @Override
    protected void onPause() {
        super.onPause();
        recorder.release();
    }

    private class AudioAdapter extends ArrayAdapter<Record> {

        private Context context;

        public AudioAdapter(Context context, int resource) {
            super(context, resource);
            this.context = context;
        }

        @Override
        public int getCount() {
            return records.size();
        }

        @Override
        public Record getItem(int position) {
            return records.get(records.size() - 1 - position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_audio_layout, parent,false);
                holder = new ViewHolder();
                holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
                holder.ivPlay = (ImageView) convertView.findViewById(R.id.bt_play);
                holder.tvSize = (TextView) convertView.findViewById(R.id.tv_size);
                holder.seekBar = (SeekBar) convertView.findViewById(R.id.seek_bar);
                holder.tvDuration = (TextView) convertView.findViewById(R.id.tv_duration);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final Record record = records.get(records.size() - 1 - position);
            holder.seekBar.setVisibility(View.GONE);
            holder.tvName.setText(record.getName());
            holder.tvDuration.setText(String.valueOf(record.getDuration()));
            holder.ivPlay.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.play));
            holder.ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int itemPlaying = checkItemPlaying();
                    if (record.isPlaying()) {
                        player.release();
                        player = null;
                        holder.ivPlay.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.play));
                        holder.seekBar.setVisibility(View.GONE);
                        record.setPlaying(false);
                    } else if (itemPlaying == -1) {
                        player = new MediaPlayer();
                        try {
                            player.setDataSource(record.getPath());
                            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    record.setPlaying(false);
                                    notifyDataSetChanged();
                                }
                            });
                            player.prepare();
                            player.start();
                            record.setPlaying(true);
                            setUpSeekBar(holder.seekBar);
                            holder.ivPlay.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.stop));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        ViewHolder vHolder = (ViewHolder) lvRecord.getChildAt(records.size() - 1 - checkItemPlaying()).getTag();
                        vHolder.ivPlay.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.play));
                        vHolder.seekBar.setVisibility(View.GONE);
                        setAllNotPlaying();
                        player.release();
                        player = null;
                        try {
                            player = new MediaPlayer();
                            player.setDataSource(record.getPath());
                            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    record.setPlaying(false);
                                    notifyDataSetChanged();}
                            });
                            player.prepare();
                            player.start();
                            record.setPlaying(true);
                            setUpSeekBar(holder.seekBar);
                            holder.ivPlay.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.stop));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            holder.tvSize.setText(String.valueOf(record.getSize()) + " kb");
            return convertView;
        }
    }

    private void setUpSeekBar(final SeekBar seekBar) {
        seekBar.setVisibility(View.VISIBLE);
        seekBar.setProgress(0);
        seekBar.setMax(player.getDuration());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (player != null) {
                    int currentDuration = player.getCurrentPosition();
                    seekBar.setProgress(currentDuration);
                    mHandler.postDelayed(this, 100);
                } else mHandler.removeCallbacks(this);
            }
        };
        mHandler.postDelayed(runnable, 100);
    }

    static class ViewHolder {
        public TextView tvName;
        public ImageView ivPlay;
        public TextView tvSize;
        public SeekBar seekBar;
        public TextView tvDuration;
    }

    private void setAllNotPlaying() {
        for (Record record : records) {
            record.setPlaying(false);
        }
    }

    private int checkItemPlaying() {
        for (int i = 0; i < records.size(); i++) {
            Record record = records.get(i);
            if (record.isPlaying()) return i;
        }
        return -1;
    }
}
