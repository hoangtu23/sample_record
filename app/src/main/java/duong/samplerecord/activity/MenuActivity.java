package duong.samplerecord.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import duong.samplerecord.R;
import duong.samplerecord.R2;

public class MenuActivity extends AppCompatActivity {

    @BindView(R2.id.bt_record)
    Button btnRecord;
    @BindView(R2.id.bt_chat)
    Button btnChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }

    @OnClick(R2.id.bt_chat)
    public void gotoChat(View view) {
        Intent chatIntent = new Intent(this, ChatActivity.class);
        startActivity(chatIntent);
    }

    @OnClick(R2.id.bt_record)
    public void gotoRecord(View view) {
        Intent recordIntent = new Intent(this, RecordActivity.class);
        startActivity(recordIntent);
    }

}
