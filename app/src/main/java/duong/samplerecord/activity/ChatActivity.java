package duong.samplerecord.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import duong.samplerecord.R;
import duong.samplerecord.R2;
import duong.samplerecord.model.MessageEntity;

public class ChatActivity extends AppCompatActivity {

    @BindView(R2.id.btn_send)
    Button btnSend;
    @BindView(R2.id.edt_content)
    EditText edtContent;
    @BindView(R2.id.recycler_view)
    RecyclerView mRecyclerView;

    private ArrayList<MessageEntity> mListMessage = new ArrayList<>();
    private MessageAdapter mAdapterMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        mAdapterMessage = new MessageAdapter(mListMessage);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapterMessage);
    }

    @OnClick(R2.id.btn_send)
    public void clickSend() {
        String message = edtContent.getText().toString();
        if (!TextUtils.isEmpty(message)) {
            mListMessage.add(0, new MessageEntity(message, System.currentTimeMillis()));
            if (mAdapterMessage != null) {
                mAdapterMessage.notifyDataSetChanged();
            }

            edtContent.setText("");
        }
    }

    /**
     * Adapter
     */
    public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
        private ArrayList<MessageEntity> messageList;

        public MessageAdapter (ArrayList<MessageEntity> _listMessage) {
            this.messageList = _listMessage;
        }

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater()
                    .inflate(R.layout.chat_row_item, parent, false);
            return new MessageViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return this.messageList != null ? this.messageList.size() : 0;
        }

        @Override
        public void onBindViewHolder(MessageViewHolder holder, int position) {
            if (this.messageList != null) {
                MessageEntity entity = messageList.get(position);
                if (entity != null) {
                    holder.message.setText(entity.getContent());
                }
            }
        }


        public class MessageViewHolder extends RecyclerView.ViewHolder {
            @BindView(R2.id.tv_message_content) TextView message;
            public MessageViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }
    }
}
